using UnityEngine;
using System.Collections;

public class pauseMenu : MonoBehaviour {

	private int buttonWidth = 200;
	private int buttonHeight = 50;
	private int groupWidth = 200;
	private int groupHeight = 170;
	bool paused = false;

	void Start ()
	{
		Screen.lockCursor = true;
		Time.timeScale = 1;
	}

	void OnGUI ()
	{
		if(paused)
		{
			GUI.BeginGroup(new Rect(((Screen.width/2) - (groupWidth/2)),((Screen.height/2) - (groupHeight/2)), groupWidth, groupHeight));
			if(GUI.Button (new Rect(0,0,buttonWidth,buttonHeight),"Restart Game"))
			{
				Application.LoadLevel(0);
			}
//			if(GUI.Button(new Rect(0,60,buttonWidth,buttonHeight),"Restart Game"))
//			{
//				Application.LoadLevel(1);
//			}
			if(GUI.Button(new Rect(0,120,buttonWidth,buttonHeight),"Quit Game"))
			{
				Application.Quit();
			}
			GUI.EndGroup();
		}
	}

	void Update ()
	{
		if(Input.GetKeyUp(KeyCode.Escape))
			paused = togglePause();
	}

	bool togglePause()
	{
		if(Time.timeScale == 0)
		{
			GameObject.Find("First Person Controller").GetComponent<MouseLook>().enabled = true;
			GameObject.Find("Main Camera").GetComponent<MouseLook>().enabled = true;
			Screen.lockCursor = true;
			Time.timeScale = 1;
			return(false);
		}
		else
		{
			GameObject.Find("First Person Controller").GetComponent<MouseLook>().enabled = false;
			GameObject.Find("Main Camera").GetComponent<MouseLook>().enabled = false;
			Screen.lockCursor = false;
			Time.timeScale = 0;
			return(true);
		}
	}
}